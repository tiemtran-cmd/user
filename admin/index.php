<?php 

    define('SYSPATH','../system/');
    
    require SYSPATH."client.php";
    
    $action = input_get('action');
    if (!$action){
        $action = "login";
    }
    require SYSPATH."admin.php";
    
    if(file_exists('action/'.$action.'.php')){
        require (SYSPATH.'database.php');
        db_connect();
         require ('action/'.$action.'.php');
         db_disconnect();
    } else {
        require ('action/error404.php');
    }
?>