<?php
    session_start();
?>
<?php if (!defined('SYSPATH')) die ('Request Not Found');?>
<!DOCTYPE html
<html>

<head>
    <meta http-equiv="content-type" content="text/html" charset="utf-8">
    <script type="text/javascript" src="../public/javascript/jquery/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="../public/javascript/summernote/summernote.js"></script>
    <script src="../public/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="../public/javascript/common.js" type="text/javascript"></script>
    <link href="../public/css/styleindex.css" rel="stylesheet" type="text/css" />
    <scscript src="javascript/javascript.js"></script>
    <link REL="SHORTCUT ICON" HREF="favicon.ico">
    <title>Manager User</title>
</head>
<body>
<div class ="wrapper">
	<div class="header">
        <ul>
            <li><a href="">Hello: <?php echo $_SESSION['username']; ?></li>
            <li> <a href="index.php?action=">Logout</li>
        </ul>
	</div>