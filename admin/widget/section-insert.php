<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../public/css/form.css" rel="stylesheet" type="text/css" />

<?php
    $action = input_post('submit_add');
    $error = array();
    if ($action == 'submit_add'){
        loab_db('admindatabase');
       $data = array(
            'username'=> input_post('insert_username'),
               'pass' => input_post('insert_password'),
               'fullname'=>input_post('insert_fullname'),
               'role' =>input_post('insert_role'),
               'email' =>input_post('insert_email')
               );
               require (SYSPATH.'validate.php');      
       
               if (validate_is_empty($data['username'])){
                   $error['username'] = 'Please Enter Your Username ';
               } else {
                   if(cate_add_username_exists($data['username'])){
                       $error['username'] ='Username Available';
                   }
               }
               if (validate_is_empty($data['pass'])){
                   $error['pass'] = 'Please Enter Password';
               }
               
               if (empty($error)){
                   $flag = cate_add($data);
                   if($flag){
                      echo '<script language="javascript">';
                      echo 'alert ("Add Success");';
                      echo 'window.location="index.php?action=listaccount"';
                      echo '</script>';
                      die();
                   }
                   
               }
               
    }
    
?>
<div id="section">
   <h1 align="center">ADD NEW USER</h1>
    <hr />
   <br />
   <a href="index.php?action=home"  style="text-decoration:none" style="font-size: 18px; padding:5px;" > AbandonChanges</a>
   <form id="frInsert" action="" method="post" name="" enctype="multipart/form-data">
       <input type="hidden" name="submit_add" value="submit_add"/>
       <label>Powers</label>
       
       <select name="insert_role">
         <option value="" selected="selected">Select Powers</option>
         <option value="1">Administrator</option>
         <option value="2">Member</option>
        
       </select>
       
       <br /><br />
       <label>Username</label>
       <input type="text" name="insert_username" value="" size="25" />
       <?php show_error($error,'username');
       ?>
       <br /><br />
       
       <label>Password</label>
       <input type="password" name="insert_password" value="" size="25" />
       <?php show_error($error,'pass'); ?>
        <br /><br />
        <label>Confirm password</label>
        <input type="password" name="insert_repassword" value="" size="25" />
        <br /><br />
        <label>Full Name</label><input type="text" name="insert_fullname" value="" size="25" />
         <br /><br />
        <label>Email</label>
        <input type="text" name="insert_email" value="" size="25" />
         <br /><br />                     
      <label>&nbsp;</label>
      <input type="submit" name="add_submit" value="Add" />&nbsp;&nbsp
   </form>
</div>