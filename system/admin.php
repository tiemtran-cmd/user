<?php
if (!defined('SYSPATH')) die ('Request Not Found');
    function load_header(){
        require('widget/header.php');
    }
    
    function load_nav(){
        require('widget/nav.php');
    }
    
    function load_widget($widget){
         require('widget/'.$widget.'.php');
        
    }
    
    function load_footer(){
        require('widget/footer.php');
    }
    
    function show_error($error,$key){
        if(isset($error[$key])){
            echo '<br/><strong style="color:red">'.$error[$key].'</strong>';
        }
    }
    function paging ($total_record,$current_page,$limit,$link){
        $page = array(
            'total_record' => intval ($total_record),
            'current_page' => intval ($current_page),
            'limit'        => intval ($limit),
            'total_page'   => 0,
            'start'        =>0,
            'html'         =>''
        );
         $page['total_page'] = ceil($page['total_record']/$page['limit']);// tong record / limit ra so page
    
        if ($page['current_page'] <1 ){
            $page['current_page'] = 1;     
        
        } else if ($page['current_page'] > $page['total_page']){
            
            $page['current_page'] = $page['total_page'];
        }
        $page ['start']     = ($page['current_page'] -1 ) *$page['limit'];// $start = ($current_page -1) *limit; start ,limit
        
        if($page['total_page'] >1){
            $page['html'] .= '<div class="pagination"><div class="links">';
            if ($page['current_page']==1){
                $page['html'] .='<b>Prev</b> ';  
            } else{
                $page['html'] .='<a href="'.str_replace('{page}',($page['current_page']-1),$link).'">Prev</a> ';    
            }
            
            for ($i =1; $i <= $page['total_page'];$i++){
                if ($page['current_page']== $i){
                     $page['html'] .='<b>'.$i.'</b> ';
                    
                }else{
                    
                    $page['html'] .='<a href="'.str_replace('{page}',$i,$link).'">'.$i.'</a> ';
                }
                
            }
            if ($page['current_page']==$page['total_page']){
                $page['html'] .='<b>Next</b> ';  
            }else{        
            $page['html'] .='<a href="'.str_replace('{page}',($page['current_page']+1),$link).'">Next</a> ';
            }
        }
        
        return $page;
        
    }
?>